import { Component, OnInit, DoCheck } from '@angular/core';
import { MessageService } from '../../services/message.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
  providers:[MessageService,UserService],
})
export class MainComponent implements OnInit, DoCheck {

  public title:string;
  public token;
  public status;
  public unviewed_messages;

  constructor(private _messageService:MessageService,private _userService:UserService) {
    this.title="Mensajes privados";
    this.token=this._userService.getToken();
    this.unviewed_messages=0;

   }

  ngOnInit() {
    console.log("Main component cargado");
    this.getUnviewedMessages();
  }

  ngDoCheck(){
    
  }

  getUnviewedMessages(){
    this._messageService.getUnviewedMessages(this.token).subscribe(
      response=>{
        console.log(response);
        if(response.unviewed!=0){
          this.status='success';
          this.unviewed_messages=response.unviewed;
          console.log("Mensajes sin leer:"+this.unviewed_messages);
        }
      },
      error=>{
        console.log(<any>error);
        this.status='error';
      }
    );
  }
}
