import { Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { from } from 'rxjs';
//modelo
import {User} from '../../models/User';
//servicio
import {UserService} from '../../services/user.service';






@Component({
    selector: 'register',
    templateUrl: './register.component.html',
    styleUrls:["./register.component.css"],
    providers:[UserService]
})

export class RegisterComponent implements OnInit{

    public title:string;
    public user:User;
    public status:string;
    public response;

    constructor(private _router:Router, private _activatedRoute:ActivatedRoute, private _userService:UserService){
        this.title='Registráte';
        this.user=new User("","","","","","","ROLE_USER","","");

    }

    ngOnInit(){
        console.log('Componente de Register cargado');
        
    }

    onSubmit(){
        //console.log(this.user);
        this._userService.register(this.user).subscribe(
            response=>{
                this.response=response;
                if(this.response.user && this.response.user._id){
                    this.status="success";
                }else{
                    this.status="error";
                }
                console.log(this.status);
            },
            error=>{
                console.log(<any>error);
            }

        );
       
    }

}