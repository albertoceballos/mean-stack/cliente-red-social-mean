import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

//servicio
import { UserService } from '../../services/user.service'
//modelos
import { User } from '../../models/User';

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
    providers:[UserService],
})

export class LoginComponent implements OnInit {

    public title: string;
    public user: User;
    public errorMessage:string;
    public status:string;
    public identity;
    public token;



    constructor(private _router: Router, private _activatedRoute: ActivatedRoute, private _userService: UserService) {
        this.title = "Identifícate";
        this.user = new User("", "", "", "", "", "", "ROLE_USER", "","");
    }

    ngOnInit() {
        console.log("Componente de Login cargado");
    }

    onSubmit(){
      //loguear al usuario:
      this._userService.signUp(this.user).subscribe(
          response=>{
            console.log(response.user);        
            this.identity=response.user;
            if(!this.identity || !this.identity._id){
              this.status='error';
            }else{
              this.status='success';    
              //lamar método getToken()
              this.getToken(); 
              //persistir datos de usuario:
              localStorage.setItem('identity',JSON.stringify(this.identity));
           
              //redirección a Home
              setTimeout(()=>{
                this._router.navigate(['/home']);
              },1000);
            }
          },
          error=>{
            console.log(<any>error);
            this.errorMessage=error.error.message;
            this.status='error';
          }
      );
    
    }

    getToken(){
      this._userService.signUp(this.user,'true').subscribe(
        response=>{
          this.token=response.token;
          if(this.token.length<=0){
            this.status='error';
          }else{
            this.status='success';
            console.log(this.token);
            //Persistir token
            localStorage.setItem('token',this.token);

            //conseguir estadísticas de usuario
            this.getCounters();

          }
        },
        error=>{
          console.log(<any>error);
          this.errorMessage=error.error.message;
          this.status='error';
        },
      );

    }

    getCounters(){
      this._userService.getCounters().subscribe(
        response=>{
          console.log(response);
          localStorage.setItem('stats',JSON.stringify(response));
        },
        error=>{
          console.log(<any>error);
        }
      );
    }


}
