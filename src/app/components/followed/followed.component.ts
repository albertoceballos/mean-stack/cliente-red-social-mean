import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

//servicios:
import { UserService } from '../../services/user.service';
import { FollowService } from '../../services/follow.service'

//modelos:
import { User } from '../../models/User';
import { Follow } from '../../models/Follow';
//ruta:
import { GLOBAL } from '../../services/global';
import { forEach } from '@angular/router/src/utils/collection';


@Component({
  selector: 'followed',
  templateUrl: './followed.component.html',
  styleUrls: ['./followed.component.css'],
  providers: [UserService, FollowService],
})
export class FollowedComponent implements OnInit {
  public url: string;
  public title: string;
  public identity;
  public token;
  public page: number;
  public nextPage: number;
  public prevPage: number;
  public status: string;
  public users: Array<User>;
  public total;
  public pages: number;
  public arrayPages: Array<number>
  public follows;
  public message: string;
  public followed;

  constructor(private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _userService: UserService,
    private _followService: FollowService) {

    this.url = GLOBAL.url;
    this.title = "Seguidores de ";
    this.identity = _userService.getIdentity();
    this.token = _userService.getToken();
    this.arrayPages = [];
    this.message = null;


  }

  ngOnInit() {
    console.log("Componente Followed cargado");
    this.actualPage();

  }

  actualPage() {

    this._activatedRoute.params.subscribe(params => {
      let userId=params['id'];
      let page = parseInt(params['page']);
      this.page = page;
      if (!page) {
        page = 1;
      } else {
        this.nextPage = page + 1;
        this.prevPage = page - 1;
        if (this.prevPage <= 0) {
          this.prevPage = 1;
        }
      }

      //devolver listado de usuarios:
     this.getUser(userId,page);
    });
  }

  getFollows(userId,page) {
    this._followService.getFollowed(this.token,userId,page).subscribe(
      response => {
        if (!response.follows) {
          this.status = 'error';
        } else {
          console.log(response);
          
          this.followed=response.follows;
          this.total=response.total;
          this.pages=response.pages;
          this.follows=response.users_following;
         
          
          this.arrayPages = [];
          for (let i = 1; i <= this.pages; i++) {
            this.arrayPages.push(i);
          }
          
          console.log("arrayPages:" + this.arrayPages);
          /*console.log(this.users);
          console.log(this.total);
          console.log(this.pages);
          console.log(this.follows);*/
          if (page > this.pages) {
            this._router.navigate(['/users', 1]);
          }
          
        }
      },
      error => {
        var errorMessage = <any>error;
        console.log(errorMessage);
        this.status = "error";
      }
    );

  }


  public user:User;
  getUser(userId,page){
    this._userService.getUser(userId).subscribe(
      response=>{
        if(response.user){
          this.user=response.user;
          this.title+=this.user.name;
          this.getFollows(userId,page);
        }else{
          this._router.navigate(['/home']);
        }
      },
      error=>{
        var errorMessage = <any>error;
        if(errorMessage!=null){
          console.log(errorMessage);
          this.status = "error";        
        }
      }
    );
  }

  public followUserOver;
  muestraSiguiendo(userId) {
    this.followUserOver = userId;
  }

  ocultaSiguiendo(userId) {
    this.followUserOver = 0;
  }


  followUser(followed) {
    var follow = new Follow('', this.identity._id, followed);

    this._followService.addFollow(this.token, follow).subscribe(
      response => {
        if (!response.follow) {
          this.status = "error";
        } else {
          this.status = "success";
          this.follows.push(followed);
        }
      },
      error => {
        let errorMessage = error;
        console.log(errorMessage);
        this.status = "error";
      }
    );
  }

  unfollowUser(followed) {
    this._followService.deleteFollow(this.token, followed).subscribe(
      response => {
        this.message = response.message;
        var i = this.follows.indexOf(followed);
        if (i != -1) {
          this.follows.splice(i, 1);
        }

        document.querySelector("#span"+followed).classList.remove('d-none');
        setTimeout(()=>{
          document.querySelector("#span"+followed).classList.add('d-none');
        },3000);
      },
      error => {
        let errorMessage = error;
        console.log(errorMessage);
        this.status = 'error';
      }
    );
  }


}


