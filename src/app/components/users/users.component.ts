import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

//servicios:
import { UserService } from '../../services/user.service';
import { FollowService } from '../../services/follow.service'

//modelos:
import { User } from '../../models/User';
import { Follow } from '../../models/Follow';
//ruta:
import { GLOBAL } from '../../services/global';
import { forEach } from '@angular/router/src/utils/collection';


@Component({
  selector: 'users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
  providers: [UserService, FollowService],
})
export class UsersComponent implements OnInit {
  public url: string;
  public title: string;
  public identity;
  public token;
  public page: number;
  public nextPage: number;
  public prevPage: number;
  public status: string;
  public users: Array<User>;
  public total;
  public pages: number;
  public arrayPages: Array<number>
  public follows;
  public message: string;

  public refrescar;

  constructor(private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _userService: UserService,
    private _followService: FollowService) {

    this.url = GLOBAL.url;
    this.title = "Gente";
    this.identity = _userService.getIdentity();
    this.token = _userService.getToken();
    this.arrayPages = [];
    this.message = null;
    this.page=1;


  }

  ngOnInit() {
    console.log("Componente Gente cargado");
    this.actualPage();

  }

  actualPage() {

    this._activatedRoute.params.subscribe(params => {
      let page = parseInt(params['page']);
      this.page = page;
      if (!page) {
        page = 1;
        this.nextPage = page + 1;
      } else {
        this.nextPage = page + 1;
        this.prevPage = page - 1;
        if (this.prevPage <= 0) {
          this.prevPage = 1;
        }
      }

      this.getUsers(page);
    });
  }

  getUsers(page) {
    this._userService.getUsers(page).subscribe(
      response => {
        if (!response.users) {
          this.status = 'error';
        } else {
          this.users = response.users;
          this.total = response.total;
          this.pages = parseInt(response.pages);
          this.follows = response.users_following;

          this.arrayPages = [];
          for (let i = 1; i <= this.pages; i++) {
            this.arrayPages.push(i);
          }

          console.log("arrayPages:" + this.arrayPages);
          console.log(this.users);
          console.log(this.total);
          console.log(this.pages);
          console.log(this.follows);
          if (page > this.pages) {
            this._router.navigate(['/users', 1]);
          }
        }
      },
      error => {
        var errorMessage = <any>error;
        console.log(errorMessage);
        this.status = "error";
      }
    );

  }

  public followUserOver;
  muestraSiguiendo(userId) {
    this.followUserOver = userId;
  }

  ocultaSiguiendo(userId) {
    this.followUserOver = 0;
  }


  followUser(followed) {
    var follow = new Follow('', this.identity._id, followed);

    this._followService.addFollow(this.token, follow).subscribe(
      response => {
        if (!response.follow) {
          this.status = "error";
        } else {
          this.status = "success";
          this.follows.push(followed);
          this._userService.getCounters().subscribe(
            response=>{
              console.log(response);
              localStorage.setItem('stats',JSON.stringify(response));
              this._router.navigate['/users/'+this.page];
            },
            error=>{
              console.log(<any>error);
            }
          );
        }
      },
      error => {
        let errorMessage = error;
        console.log(errorMessage);
        this.status = "error";
      }
    );
  }

  unfollowUser(followed) {
    this._followService.deleteFollow(this.token, followed).subscribe(
      response => {
        this.message = response.message;
        var i = this.follows.indexOf(followed);
        if (i != -1) {
          this.follows.splice(i, 1);
        }

        document.querySelector("#span"+followed).classList.remove('d-none');
        setTimeout(()=>{
          document.querySelector("#span"+followed).classList.add('d-none');
        },3000);
      },
      error => {
        let errorMessage = error;
        console.log(errorMessage);
        this.status = 'error';
      }
    );
  }


}
