import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';


import { GLOBAL } from '../../services/global';

//modelos
import { Publication } from "../../models/Publication";

//servicios
import { UserService } from '../../services/user.service';
import { PublicationService } from '../../services/publication.service';
import { UploadService } from '../../services/upload.service';


@Component({
  selector: 'sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css'],
  providers: [UserService, PublicationService, UploadService],
})
export class SidebarComponent implements OnInit {
  public title;
  public url;
  public identity;
  public token;
  public stats;
  public publication: Publication;
  public status: string;

 

  constructor(private _userService: UserService,
    private _publicationService: PublicationService,
    private _router: Router,
    private _uploadService: UploadService) {
    this.url = GLOBAL.url;
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.stats = this._userService.getStats();
    this.publication = new Publication('', '', '', '', this.identity._id);
    this.status = "";
  }

  ngOnInit() {
    console.log('Componente sidebar cargado');

    setInterval(()=>{
      this._userService.getCounters().subscribe(
        response=>{
          console.log(response);
          localStorage.setItem('stats',JSON.stringify(response));
        },
        error=>{
          console.log(<any>error);
        }
      );
      this.stats=this._userService.getStats();
    },1500);

  }

  onSubmit(form) {
    console.log(this.publication);
    this._publicationService.addPublication(this.token, this.publication).subscribe(
      response => {
        if (response.publication) {
          //this.publication=response.publication;

          if (this.filesToUpload) {
            //subir imágen
            this._uploadService.mediaFileRequest(this.url + 'upload-image-pub/' + response.publication._id, this.filesToUpload, this.token, 'image')
              .then((result: any) => {
                this.publication.file = result.image;

              });
          } 
            this.status = 'success';
            form.reset();
            //redirigir a la página de timeline
            this._router.navigate(['/timeline']);
            this.sended.emit({ send: 'true' });
          

        } else {
          this.status = 'error';
        }
      },
      error => {
        let messageError = error;
        console.log(messageError);
        this.status = 'error';
      }
    );
  }

  public filesToUpload: Array<File>;
  fileChangeEvent(files) {
    this.filesToUpload = files.target.files;
  }


  //output (para actualizar timeline al hacer publicaciones)
  @Output() sended = new EventEmitter();

  sendPublication(event) {
    this.sended.emit({ send: 'true' });
  }



  

}
