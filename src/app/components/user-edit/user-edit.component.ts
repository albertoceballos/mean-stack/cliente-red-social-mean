import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { GLOBAL } from '../../services/global';
//servicio
import { UserService } from '../../services/user.service';
import { UploadService } from '../../services/upload.service';
//modelo
import { User } from '../../models/User';

@Component({
  selector: 'user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css'],
  providers: [UserService, UploadService],
})
export class UserEditComponent implements OnInit {
  public title: string;
  public url:string;
  public user: User;
  public identity;
  public token;
  public status: string;
  public filesToUpload: Array<File>;
  public resultado;

  constructor(private _router: Router, private _activatedRoute: ActivatedRoute, private _userService: UserService,
    private _uploadService: UploadService) {

    this.title = 'Actualizar mis datos';
    this.url=GLOBAL.url;
    this.user = _userService.getIdentity();
    this.identity = this.user;
    this.token = _userService.getToken();


  }

  ngOnInit() {
    console.log('Componente user-edit cargado');
  }

  onSubmit() {
    console.log(this.user);

    this._userService.updateUser(this.user).subscribe(
      response => {
        if (!response.user) {
          this.status = 'error';
        } else {
          this.status = 'success';
          this.identity = this.user;
          localStorage.setItem('identity', JSON.stringify(this.user));

          //Subida de imagen de usuario:
          this._uploadService.mediaFileRequest(this.url+'user-image-upload/'+this.user._id,this.filesToUpload,this.token,'image')
            .then(result=>{
              this.resultado=result;
              console.log(result);
              this.user.image=this.resultado.user.image;
              localStorage.setItem('identity',JSON.stringify(this.user));
            })
        }
      },
      error => {
        let errorMessage = error;
        console.log(errorMessage);
        this.status = 'error';
      }
    );

  }


  fileChange(fileInput) {
    this.filesToUpload = fileInput.target.files;
    console.log(this.filesToUpload);
  }

}
