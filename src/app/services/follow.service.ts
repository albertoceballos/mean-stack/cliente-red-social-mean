import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable} from 'rxjs';

import {GLOBAL} from './global';
import {Follow} from '../models/Follow';

@Injectable()
export class FollowService{
    public url;

    constructor(private _httpClient:HttpClient){
        this.url=GLOBAL.url;
    }

    addFollow(token,follow):Observable<any>{
        let params=JSON.stringify(follow);
        let headers=new HttpHeaders().set('Content-type','application/json').set('Authorization',token);
    
        return this._httpClient.post(this.url+'follow',params,{headers:headers});
    }

    deleteFollow(token,userId):Observable<any>{
        let headers=new HttpHeaders().set('Content-type','application/json').set('Authorization',token);

        return this._httpClient.delete(this.url+'delete-follow/'+userId,{headers:headers});

    }

    getFollowing(token,userId=null,page=1):Observable<any>{
        let headers=new HttpHeaders().set('Content-type','application/json').set('Authorization',token);

        var url=this.url+'following';
        if(userId!=null){
            url=this.url+'following/'+userId+'/'+page;
        }
        return this._httpClient.get(url,{headers:headers});
    }

    getFollowed(token,userId=null,page=1):Observable<any>{
        let headers=new HttpHeaders().set('Content-type','application/json').set('Authorization',token);

        var url=this.url+'followed';
        if(userId!=null){
            url=this.url+'followed/'+userId+'/'+page;
        }
        return this._httpClient.get(url,{headers:headers});
    }


   
    getMyFollows(token):Observable<any>{
        let headers=new HttpHeaders().set('Content-type','application/json').set('Authorization',token);
        
        return this._httpClient.get(this.url+'get-my-follows/true',{headers:headers});
    }

}
