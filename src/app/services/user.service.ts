
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable} from 'rxjs';

import {GLOBAL} from './global';

import { User} from '../models/User';

@Injectable()
export class UserService{

    public url:string;
    public identity;
    public token;
    public stats;

    constructor(public _httpClient:HttpClient){
        this.url=GLOBAL.url;
    }

    register(user:User):Observable<any>{
        let params=JSON.stringify(user);
        let headers=new HttpHeaders().set('Content-Type','application/json');

        return this._httpClient.post(this.url+'register',params,{headers:headers});
       
    }

    signUp(user:User,getToken=null):Observable<any>{
        if(getToken!=null){
            user.getToken=getToken;
        }

        let params=JSON.stringify(user);
        let headers=new HttpHeaders().set('Content-Type','application/json');

        return this._httpClient.post(this.url+'login',params,{headers:headers});

    }

    getIdentity(){
        let identity=JSON.parse(localStorage.getItem('identity'));

        if(identity!="undefined"){
            this.identity=identity;

        }else{
            this.identity=null;
        }

        return this.identity;
    }

    getToken(){
        let token=localStorage.getItem('token');

        if(token!="undefined"){
            this.token=token;
        }else{
            this.token=null;
        }

        return this.token;
    }

    getStats(){
        let stats=JSON.parse(localStorage.getItem('stats'));
        if(stats!='undefined'){
            this.stats=stats;
        }else{
            this.stats=null;
        }

        return this.stats;
    }


    getCounters(userId=null):Observable<any>{
        let headers=new HttpHeaders().set('Content-Type','application/json').set('Authorization',this.getToken());
        if(userId!=null){
            return this._httpClient.get(this.url+'get-counters/'+userId, {headers:headers});
        }else{
            return this._httpClient.get(this.url+'get-counters', {headers:headers});
        }

    }

    updateUser(user:User):Observable<any>{
        let params=JSON.stringify(user);
        let headers=new HttpHeaders().set('Content-Type','application/json').set('Authorization',this.getToken());

        return this._httpClient.put(this.url+'user-update/'+user._id, params ,{headers:headers});


    }

    getUsers(page=null):Observable<any>{
        let headers=new HttpHeaders().set('Content-Type','application/json').set('Authorization',this.getToken());

        return this._httpClient.get(this.url+'users/'+page, {headers:headers});

    }

    getUser(id):Observable<any>{
        let headers=new HttpHeaders().set('Content-Type','application/json').set('Authorization',this.getToken());
        
        return this._httpClient.get(this.url+'user/'+id, {headers:headers});
    }



}

