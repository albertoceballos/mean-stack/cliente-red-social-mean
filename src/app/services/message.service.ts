import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { observable, Observable } from 'rxjs';
import { GLOBAL } from '../services/global';
import { Message } from '../models/Message';



@Injectable({
  providedIn: 'root'
})
export class MessageService {
  public url: string;

  constructor(private _httpClient: HttpClient) {
    this.url = GLOBAL.url;

  }

  //Enviar mensaje
  addMessage(token, message): Observable<any> {
    let params = JSON.stringify(message);
    let headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', token);

    return this._httpClient.post(this.url + 'message', params, { headers: headers });

  }

  //Mensajes recibidos
  getReceivedMessages(token, page = 1): Observable<any> {

    let headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', token);

    return this._httpClient.get(this.url + 'my-messages/' + page, { headers: headers });
  }

  //Mesnajes emitidos
  getEmittedMessages(token, page = 1): Observable<any> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', token);

    return this._httpClient.get(this.url + 'messages/' + page, { headers: headers });
  }


  getUnviewedMessages(token): Observable<any> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', token);

    return this._httpClient.get(this.url + 'unviewed-messages', { headers: headers });
  }

  removeMessage(token, messageId): Observable<any> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', token);

    return this._httpClient.delete(this.url + 'message-remove/' + messageId, { headers: headers });
  }

}
