import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import { HttpClient, HttpHeaders} from '@angular/common/http';

//url global
import {GLOBAL} from '../services/global';

//modelos
import { Publication } from '../models/Publication';


@Injectable({
  providedIn: 'root'
})
export class PublicationService {
  public url:string;

  constructor( private _httpClient:HttpClient) {
    this.url=GLOBAL.url;
   }

   addPublication(token, publication):Observable<any>{
     
    let params=JSON.stringify(publication);
    let headers=new HttpHeaders().set('Content-Type','application/json').set('Authorization',token);

    return this._httpClient.post(this.url+'publication',params,{headers:headers});

   }

   getPublications(token,page=1):Observable<any>{
    let headers=new HttpHeaders().set('Content-Type','application/json').set('Authorization',token);

    return this._httpClient.get(this.url+'publications/'+page,{headers:headers});
   }

   deletePublication(token,id):Observable<any>{
    let headers=new HttpHeaders().set('Content-Type','application/json').set('Authorization',token);

    return this._httpClient.delete(this.url+'publication-delete/'+id,{headers:headers});
    
   }

   getPublicationsUser(token,id,page=1):Observable<any>{
    let headers=new HttpHeaders().set('Content-Type','application/json').set('Authorization',token);

    return this._httpClient.get(this.url+'publications-user/'+id+'/'+page,{headers:headers});

   }
}
