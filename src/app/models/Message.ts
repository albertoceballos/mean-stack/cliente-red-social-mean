export class Message{
    public _id:string;
    public text:string;
    public viewed:string;
    public created_at:string;
    public emitter:string;
    public receiver:string;

    constructor(id,text,viewed,created,emitter,receiver){
        this._id=id;
        this.text=text;
        this.viewed=viewed;
        this.created_at=created;
        this.emitter=emitter;
        this.receiver=receiver;
    }
}