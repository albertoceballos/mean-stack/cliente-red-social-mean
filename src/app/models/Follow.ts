export class Follow{
    public _id:string;
    public user:string;
    public followed:string;

    constructor(id,user,followed){
        this._id=id;
        this.user=user;
        this.followed=followed;
    }
}